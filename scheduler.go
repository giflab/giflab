package giflab

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/peterhellberg/giphy"
	"github.com/xanzy/go-gitlab"
)

type Event struct {
	Data []byte `json:"data"`
}

var client *gitlab.Client

var (
	giflabRegexp = regexp.MustCompile(`(?i)^@giflab\s+`)
	gClient      = giphy.DefaultClient
)

// This init function is required so the client is set for the package before being
// used
func init() {
	token, found := os.LookupEnv("GIFLAB_TOKEN")
	if !found {
		panic("no GIFLAB_TOKEN")
	}

	client = gitlab.NewClient(nil, token)
}

func HandleTodos(ctx context.Context, e Event) error {
	todos, _, err := client.Todos.ListTodos(nil, nil)
	if err != nil {
		return err
	}

	for _, todo := range todos {
		log.Printf("handling todo: %d", todo.ID)

		if todo.ActionName == gitlab.TodoDirectlyAddressed || todo.ActionName == gitlab.TodoMentioned {
			if err := handleTodo(ctx, todo); err != nil {
				fmt.Printf("error handling todo %d: %v", todo.ID, err)
			}
		}

		if err := markDone(todo); err != nil {
			log.Printf("marking done failed: %v", err)
		}
	}

	return nil
}

func handleTodo(ctx context.Context, t *gitlab.Todo) error {
	query, err := detectQuery(t)
	if err != nil {
		return err
	}

	gif, err := getGif(query)
	if err != nil {
		return fmt.Errorf("unable to get a gif: %v", err)
	}

	return postGif(gif, t)
}

func getGif(query string) (*giphy.Data, error) {
	gif, err := gClient.Translate(strings.Fields(query))
	if err != nil {
		return nil, err
	}

	return &gif.Data, nil
}

func detectQuery(t *gitlab.Todo) (string, error) {
	for _, message := range []string{t.Body, t.Target.Description} {
		scanner := bufio.NewScanner(strings.NewReader(message))
		for scanner.Scan() {
			line := scanner.Bytes()
			if giflabRegexp.Match(line) {
				return string(giflabRegexp.ReplaceAll(line, []byte(""))), nil
			}
		}

		if err := scanner.Err(); err != nil {
			return "", err
		}
	}

	return "", fmt.Errorf("no query found in: %v _or_ %v", t.Body, t.Target.Description)
}

func postGif(gif *giphy.Data, t *gitlab.Todo) error {
	html := fmt.Sprintf("<img src='%s'></img>", gif.MediaURL())

	var err error
	switch t.TargetType {
	case "Issue":
		_, _, err = client.Notes.CreateIssueNote(
			t.Project.ID, t.Target.IID, &gitlab.CreateIssueNoteOptions{Body: &html},
		)
	case "MergeRequest":
		_, _, err = client.Notes.CreateMergeRequestNote(
			t.Project.ID, t.Target.IID, &gitlab.CreateMergeRequestNoteOptions{Body: &html},
		)
		// TODO: Snippets do not trigger a todo on GitLab
		// Once GitLab does, this will magically work
	case "Snippet":
		_, _, err = client.Notes.CreateMergeRequestNote(
			t.Project.ID, t.Target.IID, &gitlab.CreateMergeRequestNoteOptions{Body: &html},
		)
	default:
		log.Printf("Unsupported noteable: %s", t.Target.IID)
	}

	return err
}

func markDone(t *gitlab.Todo) error {
	_, err := client.Todos.MarkTodoAsDone(t.ID)
	return err
}
