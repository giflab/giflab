deploy: 
	go build
	gcloud --project giflab functions deploy schedular --entry-point HandleTodos --runtime go111 --trigger-topic giflab-schedule-cron --timeout=30 --memory=256MB
