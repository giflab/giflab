## GifLab

This is the code base for GifLab, and also its issue tracker. @GifLab allows
to query for gifs at GitLab.com.

Feel free to try on issues and merge requests:

```
@GifLab that's amazing
```

More information can be found [at this small website](https://giflab.gitlab.io).

#### Code

This project leverages Google Cloud functions, and has two functions:
1. Schedular
1. Gifposter

The schedular gets all the todo's and for each todo that's processable it
schedules a job for the gif poster function. All the others it marks as done.

Deploys happen locally so GifLab doesn't need to potentially expose secrets in CI
