module gitlab.com/zj/giflab

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/peterhellberg/giphy v0.0.0-20171214132724-091ba7d7516d
	github.com/xanzy/go-gitlab v0.16.2-0.20190327113132-57bae63d3a36
	golang.org/x/net v0.0.0-20190327091125-710a502c58a2 // indirect
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890 // indirect
)
